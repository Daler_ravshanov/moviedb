import {movies} from '../module/db.js'
let ul = document.querySelector('.promo__interactive-list')
let modal = document.querySelector('.modal') 
let modal_bg = document.querySelector('.modal_bg') 
let searchInp = document.querySelector('#search') 


searchInp.onkeyup = () => {
    let filtered = movies.filter(item => item.Title.toLowerCase().includes(searchInp.value.toLowerCase().trim()))

    reload(filtered)
    
}

function reload(arr) {
    ul.innerHTML = ""

    for(let item of arr) {
        let li = document.createElement('li')
        let del = document.createElement('div')

        li.classList.add('promo__interactive-item')
        del.classList.add('delete')

        li.innerHTML = item.Title

        li.append(del)
        ul.append(li)

        // functions
        li.onclick = () => {
            openModal(item)
        }

    }

}

function openModal(data) {
    // open styles
    document.body.style.overflow = "hidden"
    modal.style.display = "flex"
    modal_bg.style.display = "block"

    setTimeout(() => {
        modal.style.opacity = "1"
        modal_bg.style.opacity = "1"
        modal.style.transform = "translate(-50%, -50%) scale(1)"
    }, 300);

    // add info 
    let img = modal.querySelector('img')
    let h3 = modal.querySelector('h3')
    let span = modal.querySelector('span')
    let b = modal.querySelector('b')
    let i = modal.querySelector('i')

    h3.innerHTML = data.Title
    span.innerHTML = `Year: ${data.Year}`
    b.innerHTML = `Genres: ${data.Genre}`
    i.innerHTML = `Labguage: ${data.Language}`
    img.src = data.Poster
    
}

reload(movies)